import Vue from "vue";
import Vuex from "vuex";

import Axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {},
  state: {
    categoryList: [],
  },
  mutations: {
    SET_CATEGORY_LIST(state, payload) {
      state.categoryList = payload;
    },
  },
  actions: {
    async fetchAllCategory(context) {
      
      const headers = {
        "Content-Type": "application/json",
        // Authorization: "Bearer " + "qpmger4c12qx2t7ftfuldbl51w3js23c",
        // Store: "default",
      };

      const query = `{ categoryList(filters: {ids: {eq: "3"}}) {id level include_in_menu position url_path name children_count children {id level include_in_menu position url_path name children_count children{ id level include_in_menu position url_path name children_count children{ id level include_in_menu position url_path name children_count}}}}}`;
      const response = await Axios({
        url:
          "https://m2.antarctica.test.e-bricks.cloud/graphql" +
          "?query=" +
          encodeURI(query),
        method: "GET",
        headers: headers,
      }).catch((e) => {
        console.error(e);
      });

      const payload = response.data.data.categoryList[0].children;

      context.commit("SET_CATEGORY_LIST", payload);
    },
  },
  getters: {
    allCategories(state) {
      return state.categoryList;
    },
  },
});
